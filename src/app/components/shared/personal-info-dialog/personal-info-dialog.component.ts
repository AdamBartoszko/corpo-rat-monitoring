import {Component, OnInit} from '@angular/core';
import {User} from '../../../helpers/models/user.model';
import {UtilsService} from '../../../services/utils.service';
import {MatDialogRef} from '@angular/material';
import {UserService} from '../../../services/user.service';

@Component({
    selector: 'app-personal-info-dialog',
    templateUrl: './personal-info-dialog.component.html',
    styleUrls: ['./personal-info-dialog.component.scss']
})
export class PersonalInfoDialogComponent implements OnInit {

    currentUser: User;
    imageToShow: any;

    constructor(private utilsService: UtilsService,
                public dialogRef: MatDialogRef<PersonalInfoDialogComponent>,
                private userService: UserService) {
    }

    ngOnInit() {
        this.currentUser = this.utilsService.getCurrentUser();
        this.userService.getImageForUser(this.currentUser).subscribe(img => {
            this.createImageFromBlob(img);
        }, error => {
            console.log(error);
        });
    }

    createImageFromBlob(image: Blob) {
        const reader = new FileReader();
        reader.addEventListener('load', () => {
            this.imageToShow = reader.result;
        }, false);

        if (image) {
            reader.readAsDataURL(image);
        }
    }

    closeDialog() {
        this.dialogRef.close();
    }

}
