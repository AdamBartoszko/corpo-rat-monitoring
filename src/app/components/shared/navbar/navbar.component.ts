import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../../services/authentication.service';
import {MatDialog, MatDialogRef} from '@angular/material';
import {PersonalInfoDialogComponent} from '../personal-info-dialog/personal-info-dialog.component';
import {UtilsService} from '../../../services/utils.service';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

    userName: string;

    constructor(
        private authenticationService: AuthenticationService,
        private dialog: MatDialog,
        private utilsService: UtilsService) {
    }

    ngOnInit() {
        this.userName = this.utilsService.getCurrentUser().firstName + ' ' + this.utilsService.getCurrentUser().lastName;
    }

    logout(): void {
        this.authenticationService.logout();
    }

    openPersonalInfo(): void {
        this.dialog.open(PersonalInfoDialogComponent);
    }

}
