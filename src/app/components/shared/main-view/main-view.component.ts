import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../../services/authentication.service';
import {UtilsService} from '../../../services/utils.service';
import {UserService} from '../../../services/user.service';

@Component({
    selector: 'app-main-view',
    templateUrl: './main-view.component.html',
    styleUrls: ['./main-view.component.scss']
})
export class MainViewComponent implements OnInit {

    isUserAllowed: boolean;

    constructor(private authService: AuthenticationService,
                private utilsService: UtilsService) {
    }

    ngOnInit() {
        this.isUserAllowed = this.utilsService.isUserAllowed();
    }
}