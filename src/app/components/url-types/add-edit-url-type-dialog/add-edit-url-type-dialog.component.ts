import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormControl, Validators} from '@angular/forms';

@Component({
    selector: 'app-edit-url-type-dialog',
    templateUrl: './add-edit-url-type-dialog.component.html',
    styleUrls: ['./add-edit-url-type-dialog.component.scss']
})
export class AddEditUrlTypeDialogComponent implements OnInit {

    constructor(public dialogRef: MatDialogRef<AddEditUrlTypeDialogComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any) {
        this.editMode = data['editMode'];
        if (this.data['urlName']) {
            this.urlName = this.data['urlName'];
        }
    }

    inputControl: FormControl;
    editMode: boolean;
    urlName = 'Example name';

    ngOnInit() {
        this.inputControl = new FormControl(this.urlName, Validators.required);
    }

    addEditUrlType() {
        this.dialogRef.close(this.inputControl.value);
    }

}
