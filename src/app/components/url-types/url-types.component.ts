import {Component, OnDestroy, OnInit} from '@angular/core';
import {PagesService} from '../../services/pages.service';
import {first, takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {UrlType} from '../../helpers/models/urlType.model';
import {MatDialog, MatTableDataSource} from '@angular/material';
import {AddEditUrlTypeDialogComponent} from './add-edit-url-type-dialog/add-edit-url-type-dialog.component';
import {NotDeletedPipe} from '../../helpers/not-deleted.pipe';
import {UtilsService} from '../../services/utils.service';

@Component({
    selector: 'app-url-types',
    templateUrl: './url-types.component.html'
})

export class UrlTypesComponent implements OnInit, OnDestroy {

    dataSource: MatTableDataSource<UrlType>;
    urlTypes: UrlType[];
    notDeletedPipe = new NotDeletedPipe();
    todaysDate: string;

    displayedColumns = ['id', 'url', 'edit'];

    private ngUnsubscribe: Subject<void> = new Subject<void>();

    constructor(private pagesService: PagesService,
                private dialog: MatDialog,
                private utilsService: UtilsService) {
        this.dataSource = new MatTableDataSource(this.urlTypes);
    }

    ngOnInit() {
        this.todaysDate = this.utilsService.getTodaysDate();
        this.pagesService.getUrlTypes().pipe(takeUntil(this.ngUnsubscribe)).subscribe((urlTypes => {
            this.urlTypes = urlTypes['data'];
            this.dataSource.data = this.notDeletedPipe.transform(this.urlTypes);
        }));
    }

    ngOnDestroy(): void {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }


    editUrlType(urlType: UrlType) {
        const dialogRef = this.dialog.open(AddEditUrlTypeDialogComponent, {
            width: '300px', height: '150px',
            data: {editMode: true, urlName: urlType.name}
        });
        dialogRef.afterClosed().subscribe((name: string) => {
            if (name) {
                this.pagesService.editUrlType({
                    name: name,
                    id: urlType.id,
                    dateFrom: urlType.dateFrom,
                    dateTo: urlType.dateTo
                }).subscribe(() => {
                    this.pagesService.getUrlTypes().pipe(
                        first())
                        .subscribe(urlTypes => {
                            this.urlTypes = urlTypes['data'];
                            this.dataSource.data = this.notDeletedPipe.transform(this.urlTypes);
                        });
                });
            }
        });
    }

    deleteUrlType(urlType: UrlType): void {
        this.pagesService.deleteUrlType(urlType.id).subscribe(() => {
            this.pagesService.getUrlTypes().pipe(
                first())
                .subscribe(urlTypes => {
                    this.urlTypes = urlTypes['data'];
                    this.dataSource.data = this.notDeletedPipe.transform(this.urlTypes);
                });
        });
    }

    addUrlType(): void {
        const dialogRef = this.dialog.open(AddEditUrlTypeDialogComponent, {
            width: '300px', height: '150px',
            data: {editMode: false}
        });

        dialogRef.afterClosed().subscribe((name: string) => {
            if (name) {


                this.pagesService.addUrlType(name).subscribe(() => {
                    this.pagesService.getUrlTypes().pipe(
                        first())
                        .subscribe(urlTypes => {
                            this.urlTypes = urlTypes['data'];
                            this.dataSource.data = this.notDeletedPipe.transform(this.urlTypes);
                        });
                });
            }
        });
    }
}
