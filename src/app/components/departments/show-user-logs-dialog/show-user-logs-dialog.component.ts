import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatTableDataSource} from '@angular/material';
import {FormBuilder} from '@angular/forms';
import {User} from '../../../helpers/models/user.model';
import {UserService} from '../../../services/user.service';
import {Logs} from '../../../helpers/models/logs.model';
import saveAs from 'file-saver';

@Component({
    selector: 'app-show-user-logs-dialog',
    templateUrl: './show-user-logs-dialog.component.html',
    styleUrls: ['./show-user-logs-dialog.component.scss']
})
export class ShowUserLogsDialogComponent implements OnInit {

    constructor(private fb: FormBuilder,
                private userService: UserService,
                @Inject(MAT_DIALOG_DATA) public data: any) {
        this.user = data.user;
        this.dateTo = data.dateTo;
        this.dateFrom = data.dateFrom;
        this.dataSource = new MatTableDataSource(this.logs);
    }

    user: User;
    dateFrom: string;
    dateTo: string;
    logs: Logs[];
    dataSource: MatTableDataSource<Logs>;
    displayedColumns = ['urlType', 'timeSpent'];

    ngOnInit() {
        this.userService.getLogsForUser(this.user, this.dateFrom, this.dateTo).subscribe(logs => {
            this.logs = logs['data'];
            this.dataSource.data = this.logs;
        });
    }

    getUserLogsFile(): void {
        this.userService.getUserFile(this.user, this.dateFrom, this.dateTo).subscribe(file => {
            try {
                const isFileSaverSupported = !!new Blob;
            } catch (e) {
                console.log(e);
                return;
            }
            const blob = new Blob([file], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
            saveAs(blob, this.user.firstName + this.user.lastName + 'Report.xlsx');
        });
    }

}
