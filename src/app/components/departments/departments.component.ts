import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {DepartmentsService} from '../../services/departments.service';
import {Department} from '../../helpers/models/department.model';
import {User} from '../../helpers/models/user.model';
import {FormControl, Validators} from '@angular/forms';
import {MatDialog, MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {ShowUserLogsDialogComponent} from './show-user-logs-dialog/show-user-logs-dialog.component';
import saveAs from 'file-saver';

import * as _moment from 'moment';
import * as _rollupMoment from 'moment';

const moment = _rollupMoment || _moment;

@Component({
    selector: 'app-departments',
    templateUrl: './departments.component.html',
    styleUrls: ['./departments.component.scss']
})

export class DepartmentsComponent implements OnInit, OnDestroy {
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    constructor(private departmentsService: DepartmentsService, private dialog: MatDialog, private snackBar: MatSnackBar) {
        this.dataSource = new MatTableDataSource(this.users);
        this.dateFromControl = new FormControl(moment(['1999-01-01']));
        this.dateToControl = new FormControl(moment(['2020-01-01']));
        this.emailControl = new FormControl('', Validators.email);
    }

    dateFrom = '1999-01-01';
    dateTo = '2020-01-01';

    departments: Department[];
    dataSource: MatTableDataSource<User>;
    users: User[];

    departmentSelect: FormControl;
    selectedDepartment: string;

    dateFromControl: FormControl;
    dateToControl: FormControl;

    displayedColumns = ['id', 'firstName', 'lastName', 'email', 'logs'];

    email: string;
    emailControl: FormControl;

    private ngUnsubscribe: Subject<void> = new Subject<void>();

    ngOnDestroy(): void {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }

    ngOnInit() {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.departmentSelect = new FormControl('');
        this.departmentSelect.valueChanges.subscribe((department) => {
            this.selectedDepartment = department;
            this.dataSource.data = this.users.filter(user => user.department.name === this.selectedDepartment);
        });

        this.departmentsService.getDepartments().pipe(takeUntil(this.ngUnsubscribe)).subscribe((departments => {
            this.departments = departments['data'];
        }));

        this.departmentsService.getUsers().pipe(takeUntil(this.ngUnsubscribe)).subscribe((users => {
            this.users = users['data'];
        }));

        this.dateFromControl.valueChanges.subscribe(val => {
            this.dateFrom = val.format('YYYY-MM-DD');
        });
        this.dateToControl.valueChanges.subscribe(val => {
            this.dateTo = val.format('YYYY-MM-DD');
        });

        this.emailControl = new FormControl('', Validators.email);

        this.emailControl.valueChanges.subscribe(val => {
            this.email = val;
        });

    }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    showUserLogs(user: User) {
        const dialogRef = this.dialog.open(ShowUserLogsDialogComponent, {
            data: {user: user, dateFrom: this.dateFrom, dateTo: this.dateTo}
        });
    }

    getDepartmentLogsFile(): void {
        const department = this.departments.find(dep => dep.name === this.selectedDepartment);
        this.departmentsService.getDepartmentFile(department, this.dateFrom, this.dateTo).subscribe(file => {
            try {
                const isFileSaverSupported = !!new Blob;
            } catch (e) {
                console.log(e);
                return;
            }
            const blob = new Blob([file], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
            saveAs(blob, this.selectedDepartment + 'Report.xlsx');
        });
    }

    sendEmail(): void {
        const department = this.departments.find(dep => dep.name === this.selectedDepartment);
        this.departmentsService.sendEmail(department, this.dateFrom, this.dateTo, this.email).subscribe(res => {
            this.emailControl.reset();
            this.snackBar.open(res['message'], 'Ok', {
                duration: 2000,
            });
        });
    }

}
