import {Component, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Department} from '../../helpers/models/department.model';
import {MatTableDataSource} from '@angular/material';

import * as _moment from 'moment';
import * as _rollupMoment from 'moment';
import {DepartmentsService} from '../../services/departments.service';
import {UrlType} from '../../helpers/models/urlType.model';
import {PagesService} from '../../services/pages.service';
import {UtilsService} from '../../services/utils.service';
import {Logs} from '../../helpers/models/logs.model';

const moment = _rollupMoment || _moment;

@Component({
    selector: 'app-ranking',
    templateUrl: './ranking.component.html',
    styleUrls: ['./ranking.component.scss']
})
export class RankingComponent implements OnInit {

    dateFrom = '1999-01-01';
    dateTo = '2020-01-01';

    departments: Department[];
    dataSource: MatTableDataSource<Logs>;


    departmentSelect: FormControl;
    selectedDepartment = this.utilsService.getCurrentUser().department.name;

    types: UrlType[];
    typeSelect: FormControl;
    selectedType: string;

    dateFromControl: FormControl;
    dateToControl: FormControl;

    displayedColumns = ['rank', 'user', 'timeSpent'];

    isDepartmentSelectAllowed = this.utilsService.isUserAllowed();

    logs: Logs[];

    constructor(private departmentsService: DepartmentsService, private pagesService: PagesService, private utilsService: UtilsService) {
        this.departmentSelect = new FormControl('');
        this.typeSelect = new FormControl('');
        this.dateFromControl = new FormControl(moment(['1999-01-01']));
        this.dateToControl = new FormControl(moment(['2020-01-01']));
        this.dataSource = new MatTableDataSource(this.logs);
    }

    ngOnInit() {
        this.dateFromControl = new FormControl(moment(['1999-01-01']));
        this.dateToControl = new FormControl(moment(['2020-01-01']));

        this.departmentsService.getDepartments().subscribe((departments => {
            this.departments = departments['data'];
        }));

        this.departmentSelect.valueChanges.subscribe((department) => {
            this.selectedDepartment = department;
        });

        this.typeSelect.valueChanges.subscribe((type) => {
            this.selectedType = type;
        });

        this.pagesService.getUrlTypes().subscribe(val => {
            this.types = val['data'];
        });

        this.dateFromControl.valueChanges.subscribe(val => {
            this.dateFrom = val.format('YYYY-MM-DD');
        });
        this.dateToControl.valueChanges.subscribe(val => {
            this.dateTo = val.format('YYYY-MM-DD');
        });
    }

    generateRanking() {
        const department = this.departments.find(dep => dep.name === this.selectedDepartment);
        this.departmentsService.getDepartmentLogs(department, this.dateFrom, this.dateTo).subscribe(val => {
            this.logs = val['data'].sort((log1: Logs, log2: Logs) => {
                return this.convertTimeSpent(log1.timeSpent) - this.convertTimeSpent(log2.timeSpent)
            }).filter(x => x.type === this.selectedType);
            this.dataSource.data = this.logs;
        });
    }

    private convertTimeSpent(timeSpent: string): number {
        let convertedTime = timeSpent.replace('dni', '');
        convertedTime = convertedTime.replace('godz.', '');
        convertedTime = convertedTime.replace('min.', '');
        convertedTime = convertedTime.replace(/ +(?= )/g, '');
        convertedTime = convertedTime.slice(0, -1);

        const array = convertedTime.split(' ');

        return Number(array[0]) * 1440 + Number(array[1]) * 60 + Number(array[2]);
    }

}
