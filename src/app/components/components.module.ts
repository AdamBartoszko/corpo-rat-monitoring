import {NgModule} from '@angular/core';
import {MainViewComponent} from './shared/main-view/main-view.component';
import {LoginComponent} from './login/login.component';
import {HomeComponent} from './home/home.component';
import {ErrorComponent} from './shared/error/error.component';
import {PersonalInfoDialogComponent} from './shared/personal-info-dialog/personal-info-dialog.component';
import {NavbarComponent} from './shared/navbar/navbar.component';
import {UrlTypesComponent} from './url-types/url-types.component';
import {AddEditUrlTypeDialogComponent} from './url-types/add-edit-url-type-dialog/add-edit-url-type-dialog.component';
import {DepartmentsComponent} from './departments/departments.component';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
    MatButtonModule,
    MatCardModule, MatDatepickerModule, MatDialogModule,
    MatIconModule,
    MatInputModule,
    MatListModule, MatNativeDateModule, MatPaginatorModule,
    MatSelectModule, MatSidenavModule, MatSnackBarModule, MatTableModule,
    MatTabsModule, MatToolbarModule, MatTooltipModule
} from '@angular/material';
import {RouterModule} from '@angular/router';
import {NotDeletedPipe} from '../helpers/not-deleted.pipe';
import { PatternsComponent } from './patterns/patterns.component';
import { AddEditPatternDialogComponent } from './patterns/add-edit-pattern-dialog/add-edit-pattern-dialog.component';
import { RankingComponent } from './ranking/ranking.component';
import { ShowUserLogsDialogComponent } from './departments/show-user-logs-dialog/show-user-logs-dialog.component';


@NgModule({
    declarations: [
        MainViewComponent,
        LoginComponent,
        HomeComponent,
        ErrorComponent,
        NavbarComponent,
        UrlTypesComponent,
        PersonalInfoDialogComponent,
        AddEditUrlTypeDialogComponent,
        DepartmentsComponent,
        NotDeletedPipe,
        PatternsComponent,
        AddEditPatternDialogComponent,
        RankingComponent,
        ShowUserLogsDialogComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        MatInputModule,
        MatButtonModule,
        MatSelectModule,
        MatIconModule,
        MatCardModule,
        MatListModule,
        MatTabsModule,
        MatSnackBarModule,
        MatSidenavModule,
        MatToolbarModule,
        MatTooltipModule,
        MatDialogModule,
        MatTableModule,
        MatPaginatorModule,
        MatDatepickerModule,
        MatNativeDateModule,
        RouterModule,
    ],
    exports: []
})
export class ComponentsModule {
}
