import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UrlType} from '../../../helpers/models/urlType.model';
import {Pattern} from '../../../helpers/models/pattern.model';

@Component({
    selector: 'app-add-edit-pattern-dialog',
    templateUrl: './add-edit-pattern-dialog.component.html',
    styleUrls: ['./add-edit-pattern-dialog.component.scss']
})
export class AddEditPatternDialogComponent implements OnInit {
    constructor(public dialogRef: MatDialogRef<AddEditPatternDialogComponent>,
                private fb: FormBuilder,
                @Inject(MAT_DIALOG_DATA) public data: any) {
        this.editMode = data['editMode'];
        this.urlTypes = data['urlTypes'];
        this.selectedUrlType = data['urlType'];

        if (this.data['pattern']) {
            this.selectedPattern = this.data['pattern'];
            this.patternUrl = this.selectedPattern.url;
            this.selectedUrlType = this.selectedPattern.type;
        } else {
            this.selectedPattern = null;
            this.patternUrl = 'example.pl';
            this.selectedUrlType = this.data['urlTypes'][0];
        }
    }

    inputControl: FormGroup;
    editMode: boolean;
    patternUrl: string;
    urlTypes: UrlType[];
    selectedUrlType: UrlType;
    selectedPattern: Pattern;

    ngOnInit() {
        this.inputControl = this.fb.group({
            url: [this.patternUrl, Validators.required],
            type: [this.selectedUrlType.name, Validators.required]
        });

        this.inputControl.valueChanges.subscribe(values => {
            this.selectedUrlType = this.urlTypes.find(e => e.name === values['type']);
        });
    }

    addEditPattern() {
        if (this.editMode) {
            this.dialogRef.close({
                id: this.selectedPattern.id,
                url: this.inputControl.get('url').value,
                dateFrom: this.selectedPattern.dateFrom,
                type: {id: this.selectedUrlType.id}
            });
        } else {
            this.dialogRef.close({
                url: this.inputControl.get('url').value,
                type: {id: this.selectedUrlType.id}
            });
        }
    }

}
