import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {FormControl} from '@angular/forms';
import {Pattern} from '../../helpers/models/pattern.model';
import {UrlType} from '../../helpers/models/urlType.model';
import {PagesService} from '../../services/pages.service';
import {Subject} from 'rxjs';
import {first, takeUntil} from 'rxjs/operators';
import {NotDeletedPipe} from '../../helpers/not-deleted.pipe';
import {AddEditPatternDialogComponent} from './add-edit-pattern-dialog/add-edit-pattern-dialog.component';
import {UtilsService} from '../../services/utils.service';

@Component({
    selector: 'app-patterns',
    templateUrl: './patterns.component.html'
})
export class PatternsComponent implements OnInit, OnDestroy {
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    constructor(private pagesService: PagesService,
                private dialog: MatDialog,
                private utilsService: UtilsService) {
        this.dataSource = new MatTableDataSource(this.patterns);
    }

    urlTypes: UrlType[];
    dataSource: MatTableDataSource<Pattern>;
    patterns: Pattern[];
    notDeletedPipe = new NotDeletedPipe();
    urlTypeSelect: FormControl;
    selectedUrlType: string;
    todaysDate: string;

    displayedColumns = ['id', 'url', 'edit'];

    private ngUnsubscribe: Subject<void> = new Subject<void>();

    ngOnDestroy(): void {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }

    ngOnInit() {
        this.todaysDate = this.utilsService.getTodaysDate();
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.urlTypeSelect = new FormControl('');
        this.urlTypeSelect.valueChanges.subscribe((department) => {
            this.selectedUrlType = department;
            this.dataSource.data = this.patterns.filter(pattern => pattern.type.name === this.selectedUrlType);
        });

        this.pagesService.getUrlTypes().pipe(takeUntil(this.ngUnsubscribe)).subscribe((urlTypes => {
            this.urlTypes = urlTypes['data'];
        }));

        this.pagesService.getPatterns().pipe(takeUntil(this.ngUnsubscribe)).subscribe((patterns => {
            this.patterns = patterns['data'];
        }));
    }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    editPattern(pattern: Pattern) {
        const dialogRef = this.dialog.open(AddEditPatternDialogComponent, {
            width: '300px', height: '230px',
            data: {editMode: true, urlTypes: this.notDeletedPipe.transform(this.urlTypes), pattern: pattern}
        });
        dialogRef.afterClosed().subscribe(value => {
            if (value) {
                this.pagesService.editPattern(value).subscribe(() => {
                    this.pagesService.getPatterns().pipe(
                        first())
                        .subscribe(patterns => {
                            this.patterns = patterns['data'];
                            this.dataSource.data = this.notDeletedPipe.transform(this.patterns).filter(pat => pat.type.name === this.selectedUrlType);
                        });
                });

            }
        });
    }

    addPattern(): void {
        const dialogRef = this.dialog.open(AddEditPatternDialogComponent, {
            width: '300px', height: '230px',
            data: {editMode: false, urlTypes: this.notDeletedPipe.transform(this.urlTypes)}
        });

        dialogRef.afterClosed().subscribe(value => {
            if (value) {
                this.pagesService.addPattern(value).subscribe(() => {
                    this.pagesService.getPatterns().pipe(
                        first())
                        .subscribe(patterns => {
                            this.patterns = patterns['data'];
                            this.dataSource.data = this.notDeletedPipe.transform(this.patterns).filter(pat => pat.type.name === this.selectedUrlType);
                        });
                });
            }
        });
    }

    deletePattern(pattern: Pattern): void {
        this.pagesService.deletePattern(pattern).subscribe(() => {
            this.pagesService.getPatterns().pipe(
                first())
                .subscribe(patterns => {
                    this.patterns = patterns['data'];
                    this.dataSource.data = this.notDeletedPipe.transform(this.patterns).filter(pat => pat.type.name === this.selectedUrlType);
                });
        });
    }

    checkIfOlder(pattern: Pattern): boolean {
        return new Date(pattern.dateFrom.substring(0, 10)) < new Date(this.todaysDate);
    }

}
