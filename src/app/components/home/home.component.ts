import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {UserService} from '../../services/user.service';
import {UtilsService} from '../../services/utils.service';
import {first} from 'rxjs/operators';
import {MatSnackBar, MatTableDataSource} from '@angular/material';
import {Logs} from '../../helpers/models/logs.model';
import saveAs from 'file-saver';

import * as _moment from 'moment';
import * as _rollupMoment from 'moment';

const moment = _rollupMoment || _moment;

@Component({
    selector: 'app-test',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    dateFromControl: FormControl;
    dateToControl: FormControl;

    dateFrom = '1999-01-01';
    dateTo = '2020-01-01';
    logs: Logs[];
    dataSource: MatTableDataSource<Logs>;
    displayedColumns = ['urlType', 'timeSpent'];

    email: string;
    emailControl: FormControl;


    constructor(private fb: FormBuilder,
                private userService: UserService,
                private utilsService: UtilsService,
                private snackBar: MatSnackBar) {
        this.dateToControl = new FormControl(moment('2020-01-01'));
        this.dateFromControl = new FormControl(moment('1999-01-01'));
        this.dataSource = new MatTableDataSource(this.logs);
        this.emailControl = new FormControl('', Validators.email);
    }

    ngOnInit() {
        this.dateFromControl.valueChanges.subscribe(val => {
            this.dateFrom = val.format('YYYY-MM-DD');
        });
        this.dateToControl.valueChanges.subscribe(val => {
            this.dateTo = val.format('YYYY-MM-DD');
        });

        this.userService.getLogsForUser(this.utilsService.getCurrentUser(), '1999-01-01', '2020-01-01')
            .pipe(first())
            .subscribe((logs) => {
                this.logs = logs['data'];
                this.dataSource.data = this.logs;
            });

        this.emailControl.valueChanges.subscribe(val => {
            this.email = val;
        });
    }

    displayLogs(): void {
        this.userService.getLogsForUser(this.utilsService.getCurrentUser(), this.dateFrom, this.dateTo)
            .pipe(first())
            .subscribe((logs) => {
                this.logs = logs['data'];
                this.dataSource.data = this.logs;
            });
    }

    getPersonalLogsFile(): void {
        this.userService.getUserFile(this.utilsService.getCurrentUser(), this.dateFrom, this.dateTo).subscribe(file => {
            try {
                const isFileSaverSupported = !!new Blob;
            } catch (e) {
                console.log(e);
                return;
            }
            const blob = new Blob([file], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
            saveAs(blob, 'PersonalReport.xlsx');
        });
    }

    sendEmail() {
        this.userService.sendEmail(this.utilsService.getCurrentUser(), this.dateFrom, this.dateTo, this.email).subscribe(res => {
            this.emailControl.reset();
            this.snackBar.open(res['message'], 'Ok', {
                duration: 2000,
            });
        });
    }

}
