import {RouterModule, Routes} from '@angular/router';
import {MainViewComponent} from './components/shared/main-view/main-view.component';
import {LoginComponent} from './components/login/login.component';
import {AuthGuard} from './guards/auth.guard';
import {ErrorComponent} from './components/shared/error/error.component';
import {UrlTypesComponent} from './components/url-types/url-types.component';
import {PermissionGuard} from './guards/permission.guard';
import {DepartmentsComponent} from './components/departments/departments.component';
import {PatternsComponent} from './components/patterns/patterns.component';
import {HomeComponent} from './components/home/home.component';
import {RankingComponent} from './components/ranking/ranking.component';

const appRoutes: Routes = [
    {
        path: '', component: MainViewComponent, canActivate: [AuthGuard], children: [
            {path: 'home', component: HomeComponent},
            {path: 'patterns', component: PatternsComponent, canActivate: [PermissionGuard]},
            {path: 'url-types', component: UrlTypesComponent, canActivate: [PermissionGuard]},
            {path: 'department', component: DepartmentsComponent, canActivate: [PermissionGuard]},
            {path: 'ranking', component: RankingComponent}]
    },
    {path: 'login', component: LoginComponent},

    // otherwise redirect to home
    {path: '**', component: ErrorComponent}
];

export const routing = RouterModule.forRoot(appRoutes);
