import {NgModule} from '@angular/core';
import {AppComponent, MY_FORMATS} from './app.component';
import {BrowserModule} from '@angular/platform-browser';
import {ComponentsModule} from './components/components.module';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {routing} from './app.routing';
import {JwtInterceptor} from './helpers/jwt.interceptor';
import {PersonalInfoDialogComponent} from './components/shared/personal-info-dialog/personal-info-dialog.component';
import {AddEditUrlTypeDialogComponent} from './components/url-types/add-edit-url-type-dialog/add-edit-url-type-dialog.component';
import {AddEditPatternDialogComponent} from './components/patterns/add-edit-pattern-dialog/add-edit-pattern-dialog.component';
import {ShowUserLogsDialogComponent} from './components/departments/show-user-logs-dialog/show-user-logs-dialog.component';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material';
import {MomentDateAdapter} from '@angular/material-moment-adapter';


@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        ComponentsModule,
        HttpClientModule,
        routing
    ],
    providers: [
        {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
        {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS}
    ],
    entryComponents: [
        PersonalInfoDialogComponent,
        AddEditUrlTypeDialogComponent,
        AddEditPatternDialogComponent,
        ShowUserLogsDialogComponent
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
