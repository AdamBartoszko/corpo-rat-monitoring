import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map, mergeMap} from 'rxjs/operators';
import {appConfig} from '../config';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {
    constructor(private http: HttpClient) {
    }

    login(login: string, password: string) {
        return this.http.post<any>(appConfig.login, {login, password})
            .pipe(mergeMap(userInfo => {
                if (userInfo && userInfo.data.token) {
                    localStorage.setItem('userInfo', JSON.stringify(userInfo));
                    return this.http.get<any>(appConfig.getCurrentUser, {params: {id: userInfo.data.id}}).pipe(
                        map(user => {
                            localStorage.setItem('currentUser', JSON.stringify(user));
                        })
                    );
                }
            }));
    }

    logout() {
        localStorage.removeItem('userInfo');
        localStorage.removeItem('currentUser');
    }
}
