import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {appConfig} from '../config';
import {map} from 'rxjs/operators';
import {UtilsService} from './utils.service';
import {UrlType} from '../helpers/models/urlType.model';
import {Pattern} from '../helpers/models/pattern.model';

@Injectable({
    providedIn: 'root'
})
export class PagesService {
    constructor(private http: HttpClient,
                private utilsService: UtilsService) {
    }

    getUrlTypes() {
        return this.http.get(appConfig.getUrlTypes)
            .pipe(map((urlTypes) => {
                return urlTypes;
            }));
    }

    deleteUrlType(id: string) {
        return this.http.request('delete', appConfig.deleteUrl, {body: {id: id}})
            .pipe(map(() => {
                return true;
            }));
    }

    addUrlType(name: string) {
        return this.http.post(appConfig.addUrl, {name: name, from: this.utilsService.getTodaysDate()})
            .pipe(map(() => {
                return true;
            }));
    }

    editUrlType(urlType: UrlType) {
        return this.http.put(appConfig.editUrl, {id: urlType.id, name: urlType.name, from: urlType.dateFrom});
    }

    getPatterns() {
        return this.http.get(appConfig.getPatterns)
            .pipe(map((patterns) => {
                return patterns;
            }));
    }

    addPattern(pattern: Pattern) {
        return this.http.post(appConfig.addPattern, {
            url: pattern.url,
            from: this.utilsService.getTodaysDate(),
            type: pattern.type
        })
            .pipe(map(() => {
                return true;
            }));
    }

    editPattern(pattern: Pattern) {
        return this.http.put(appConfig.editPattern, {
            id: pattern.id,
            url: pattern.url,
            from: this.utilsService.getTodaysDate(),
            type: pattern.type
        })
            .pipe(map(() => {
                return true;
            }));
    }

    deletePattern(pattern: Pattern) {
        return this.http.request('delete', appConfig.deletePattern, {body: {id: pattern.id}})
            .pipe(map(() => {
                return true;
            }));
    }
}
