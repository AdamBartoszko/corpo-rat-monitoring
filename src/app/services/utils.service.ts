import {Injectable} from '@angular/core';
import {User} from '../helpers/models/user.model';

@Injectable({
    providedIn: 'root'
})
export class UtilsService {
    isUserAllowed(): boolean {
        return JSON.parse(localStorage.getItem('userInfo')).data.auth.length === 2;
    }

    getCurrentUser(): User {
        return JSON.parse(localStorage.getItem('currentUser')).data;
    }

    getTodaysDate(): string {
        const rightNow = new Date();
        const res = rightNow.toISOString().slice(0, 10).replace(/-/g, '-');
        return res;
    }
}
