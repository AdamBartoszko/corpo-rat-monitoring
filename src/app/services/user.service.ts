import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {appConfig} from '../config';
import {Observable} from 'rxjs';
import {User} from '../helpers/models/user.model';

@Injectable({
    providedIn: 'root'
})
export class UserService {
    constructor(private http: HttpClient) {
    }

    getImageForUser(user: User): Observable<Blob> {
        return this.http.get(appConfig.getImage, {responseType: 'blob', params: {id: '4'}});
    }

    getLogsForUser(user: User, dateFrom: string, dateTo: string) {
        return this.http.get(appConfig.getUserLogs, {
            params: {
                id: user.id.toString(),
                from: dateFrom,
                to: dateTo
            }
        });
    }

    getUserFile(user: User, dateFrom: string, dateTo: string) {
        return this.http.get(appConfig.getUserRawLogs, {responseType: 'blob',
            params: {
                id: user.id.toString(),
                from: dateFrom,
                to: dateTo,
                file: 'xlsx'
            }
        });
    }

    sendEmail(user: User, dateFrom: string, dateTo: string, email: string) {
        return this.http.get(appConfig.getUserRawLogs, {
            params: {
                id: user.id.toString(),
                from: dateFrom,
                to: dateTo,
                file: 'email',
                address: email
            }
        });
    }
}