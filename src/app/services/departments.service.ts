import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {appConfig} from '../config';
import {Department} from '../helpers/models/department.model';

@Injectable({
    providedIn: 'root'
})
export class DepartmentsService {
    constructor(private http: HttpClient) {
    }

    getDepartments() {
        return this.http.get(appConfig.getDepartments)
            .pipe(map((departments) => {
                return departments;
            }));
    }

    getUsers() {
        return this.http.get(appConfig.getUsers)
            .pipe(map(users => {
                return users;
            }));
    }

    getDepartmentLogs(department: Department, dateFrom: string, dateTo: string) {
        return this.http.get(appConfig.getDepartmentUsersLogs, {
            params: {
                id: department.id.toString(),
                from: dateFrom,
                to: dateTo
            }
        });
    }

    getDepartmentFile(department: Department, dateFrom: string, dateTo: string) {
        return this.http.get(appConfig.getDepartmentRawLogs, {
            responseType: 'blob',
            params: {
                id: department.id.toString(),
                from: dateFrom,
                to: dateTo,
                file: 'xlsx'
            }
        });
    }


    sendEmail(department: Department, dateFrom: string, dateTo: string, email: string) {
        return this.http.get(appConfig.getDepartmentRawLogs, {
            params: {
                id: department.id.toString(),
                from: dateFrom,
                to: dateTo,
                file: 'email',
                address: email
            }
        });
    }
}
