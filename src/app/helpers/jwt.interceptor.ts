import {Injectable} from '@angular/core';
import {HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {MatSnackBar} from '@angular/material';
import {tap} from 'rxjs/operators';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

    constructor(public snackBar: MatSnackBar) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const userInfo = JSON.parse(localStorage.getItem('userInfo'));
        if (userInfo && userInfo.data.token) {
            request = request.clone({
                setHeaders: {
                    'Content-Type': 'application/json',
                    'access-token': userInfo.data.token
                }
            });
        }

        return next.handle(request).pipe(tap((event: HttpEvent<any>) => {
        }, (err: any) => {
            if (err instanceof HttpErrorResponse) {
                if (err.error.data) {
                    this.snackBar.open(err.error.data[0], 'Ok', {
                        duration: 2000
                    });
                } else {
                    this.snackBar.open(err.statusText, 'Ok', {
                        duration: 2000
                    });
                }
            }
        }));
    }
}
