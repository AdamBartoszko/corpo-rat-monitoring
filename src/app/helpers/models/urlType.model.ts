export class UrlType {
    id: string;
    name: string;
    dateFrom: string;
    dateTo: string;
}