import {Department} from './department.model';

export class User {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    department: Department;
}
