import {UrlType} from './urlType.model';

export class Pattern {
    id: number;
    url: string;
    dateFrom: string;
    dateTo: string;
    type: UrlType;
}
