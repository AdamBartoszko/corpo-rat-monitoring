export class Logs {
    email: string;
    timeSpent: string;
    type: string;
    username: string;
}