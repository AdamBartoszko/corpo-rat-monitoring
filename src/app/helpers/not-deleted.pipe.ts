import { Pipe, PipeTransform } from '@angular/core';
import {UrlType} from './models/urlType.model';

@Pipe({
  name: 'notDeleted'
})
export class NotDeletedPipe implements PipeTransform {

  transform(array): any {
    if (array) {
        return array.filter(type => type.dateTo === null);
    }
  }
}
